if (process.env.NODE_ENV === "production")
    require("newrelic");

const PORT = process.env.PORT || 3333;

import os from "os";
import express from "express";
import http from "http";
import RoutesConfig from "./config/routes.conf";
import DBConfig from "./config/db.conf";
import Routes from "./routes/index";
let request = require('request');


let bodyParser = require('body-parser');
/*let urlencodedParser = bodyParser.urlencoded({ extended: false });*/
const app = express();
let urlencodedParser = bodyParser.urlencoded({ extended: false });
app.use(urlencodedParser);
app.use(bodyParser.json());

app.post('/api/messages', urlencodedParser, (req, res) =>{
  res.status(200).end(); // best practice to respond with 200 status
  let actionJSONPayload = JSON.parse(req.body.payload); // parse URL-encoded payload JSON string
  let message = {
    "text": actionJSONPayload.user.name+" clicked: "+actionJSONPayload.actions[0].name,
    "replace_original": false
  };
  sendMessageToSlackResponseURL(actionJSONPayload.response_url, message)
});

function sendMessageToSlackResponseURL(responseURL, JSONmessage){
  let postOptions = {
    uri: responseURL,
    method: 'POST',
    headers: {
      'Content-type': 'application/json'
    },
    json: JSONmessage
  };
  request(postOptions, (error, response, body) => {
    if (error){
      // handle errors as you see fit
    }
  })
}

RoutesConfig.init(app);
// DBConfig.init();
Routes.init(app, express.Router());

http.createServer(app)
    .listen(PORT, () => {
      console.log(`up and running @: ${os.hostname()} on port: ${PORT}`);
      console.log(`enviroment: ${process.env.NODE_ENV}`);
    });
