import oauthDAO from '../dao/oauth-dao';

export default class oauthController {
  static getAuth(req, res) {
    oauthDAO
      .getAuth()
      .then(oauths => res.status(200).json(oauths))
      .catch(error => res.status(400).json(error));
  }
  static redirectPermissions(req, res) {
    oauthDAO
      .redirectPermissions(req, res)
      .then(oauths => res.status(200).json(oauths))
      .catch(error => res.status(400).json(error));
  }
  static sendMessage(req, res) {
    oauthDAO
      .sendMessage(req, res)
      .then(resp => res.status(200).json(resp))
      .catch(error => res.status(400).json(error));
  }
}
