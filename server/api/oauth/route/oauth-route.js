import oauthController from '../controller/oauth-controller';

export default class oauthRoutes {
  static init(router) {
    router
      .route('/api/oauth')
      .get(oauthController.getAuth);
    router
      .route('/auth/redirect')
      .get(oauthController.redirectPermissions);
    /*router
      .route('/api/messages')
      .post(oauthController.sendMessage);*/
  }
}
