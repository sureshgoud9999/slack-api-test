import Promise from "bluebird";
import {loadExtProp} from "../../../utils/serverUtils";
import request from 'request';

export default class OauthDao{

  static getAuth(request){
    return new Promise((resolve,reject)=>{
      resolve({"CLIENT_ID" : loadExtProp('CLIENT_ID'),
        USER_ID: loadExtProp('CLIENT_SECRET'),"REDIRECT_URI": loadExtProp('REDIRECT_URI')})
    })
  }
  static redirectPermissions(req, res){
    return new Promise((resolve,reject)=>{
      try {
        if (req.query && req.query.code) {
          let options = {
            uri: 'https://slack.com/api/oauth.access?code='
            + req.query.code +
            '&client_id=' + loadExtProp('CLIENT_ID') +
            '&client_secret=' + loadExtProp('CLIENT_SECRET') +
            '&redirect_uri=' + loadExtProp('REDIRECT_URI'),
            method: 'GET'
          };
          console.log('Sending request with options: ', options);
          request(options, (error, response, body) => {
            let JSONresponse = JSON.parse(body);
            console.log('Got the response: ', JSONresponse);
            if (!JSONresponse.ok) {
              console.log(JSONresponse);
              reject("Error encountered: \n" + JSON.stringify(JSONresponse))
            } else {
              console.log(JSONresponse);
              resolve(JSONresponse);
            }
          });
        } else {
          console.log('No code found in request... ');
          reject('Request is not valid...!');
        }
      } catch(error) {
        console.log('--Error occured while processing ...', error);
      }
    })
  }
  static sendMessage(req, res){
    return new Promise((resolve,reject)=>{
      try {
        console.log("got the message from slack...");
        console.log('Sending request with options: ', req);
          resolve("got the message from slack...")
      } catch(error) {
        console.log('--Error occured while processing ...', error);
      }
    })
  }
}
