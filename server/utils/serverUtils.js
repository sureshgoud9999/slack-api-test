import * as _ from 'lodash'
const EXT_PROPS = require('../../slack-api-ext.json')

export function loadExtProp (property) {
  return process.env[property] ? process.env[property] : EXT_PROPS[property]
}
