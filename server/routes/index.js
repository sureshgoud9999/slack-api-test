import OAuthRoutes from "../api/oauth/route/oauth-route";


export default class Routes {
   static init(app, router) {
     OAuthRoutes.init(router);


     app.use("/", router);
   }
}
